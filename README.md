# OpenML dataset: ar1

https://www.openml.org/d/1059

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
This is a PROMISE Software Engineering Repository data set made publicly
available in order to encourage repeatable, refutable, verifiable, and/or
improvable predictive models of software engineering.

If you publish material based on PROMISE data sets then, please follow
the acknowledgment guidelines posted on the PROMISE repository web page
http://promise.site.uottowa.ca/SERepository.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
--Title: AR1 /Software Defect Prediction
--Date: February, 4th, 2009
--Data from a Turkish white-goods manufacturer
--Donated by: Software Research Laboratory (Softlab),
Bogazici University, Istanbul, Turkey
--Website: http://softlab.boun.edu.tr
--Contact address: ayse.tosun@boun.edu.tr, bener@boun.edu.tr

--Description:
Embedded software in a white-goods product.
Implemented in C.
Consists of 121 modules (9 defective / 112 defect-free)
29 static code attributes (McCabe, Halstead and LOC measures) and 1 defect information(false/true)
Function/method level static code attributes are collected using
Prest Metrics Extraction and Analysis Tool [1].
[1] Prest Metrics Extraction and Analysis Tool, available at http://softlab.boun.edu.tr/?q=resources&i=tools.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1059) of an [OpenML dataset](https://www.openml.org/d/1059). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1059/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1059/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1059/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

